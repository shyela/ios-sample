//
//  AppDelegate.h
//  ShySample
//
//  Created by Shy Aberman on 6/11/15.
//  Copyright (c) 2015 General UI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

