//
//  ViewController.m
//  ShySample
//
//  Created by Shy Aberman on 6/11/15.
//  Copyright (c) 2015 General UI. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.statesColorMap setColor: [ UIColor colorWithRed: 0.5 green: 0 blue: 0 alpha: 1 ] forState: Washington];
    
    [self.button addTarget:self
                    action:@selector(event_button_click:)
          forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)event_button_click:(id)sender
{
    self.statesColorMap.hidden = !self.statesColorMap.hidden;
}

@end
