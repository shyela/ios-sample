//
//  ViewController.h
//  ShySample
//
//  Created by Shy Aberman on 6/11/15.
//  Copyright (c) 2015 General UI. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "USStatesColorMap/USStatesColorMap.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet USStatesColorMap *statesColorMap;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

